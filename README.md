# cleaning.dumps

## Background

Example project for the [EclipseCon 2021](https://www.eclipsecon.org/2021) session: ["Automated testing of OpenAPI-described RESTful microservices utilizing open source tools"](https://www.eclipsecon.org/2021/sessions/automated-testing-openapi-described-restful-microservices-utilizing-open-source-tools).

This example project relies on my yet unreleased `k6` OpenAPI Generator contributions: 
 
 - [[ K6 Generator ] Support for extracting examples defined at parameter level of Swagger/OpenAPI specification, plus minor fixes #9750](https://github.com/OpenAPITools/openapi-generator/pull/9750)
 - [[ K6 Generator ] Further K6 OpenAPI generator enhancements (request body example data extraction, support for generating scenario tests and load tests out of the box, and much more) #10614](https://github.com/OpenAPITools/openapi-generator/pull/10614)

i.e.:

- Parameter example data extraction
- Request body example data extraction
- Request grouping and ordering (`x-k6-openapi-operation-grouping` custom OpenAPI extension)
- Response visibility (`x-k6-openapi-operation-response` custom OpenAPI extension)
- Request data extraction for chaining requests (`x-k6-openapi-operation-dataextract` custom OpenAPI extension)

OpenAPI / Swagger specification is utilized as the single source of truth for automated REST API test generation, including extracting of test data from examples embedded in OpenAPI / Swagger spec.

REST API test scripts are generated based on the OpenAPI / Swagger spec via the `generate-tests.sh` script. It is a wrapper script for the enhanced `k6` OpenAPI code generator with customized templates.

In contrast to the original `k6` OpenAPI code generator and the templates included with it, which produce "usable skeletons" requiring significant additional manual modifications before use, the enhanced `k6` OpenAPI code generator with customized templates produces scripts which are ready to run, out-of-the-box. With these enhancements and customizations, currently [smoke tests](https://k6.io/docs/test-types/smoke-testing/), [load tests](https://k6.io/docs/test-types/load-testing/) as well [user flow (scenario) tests](https://k6.io/docs/testing-guides/api-load-testing/#user-flow-scenarios-testing) can be generated automatically.

Utilizing OpenAPI generator tooling allows for full round-trip development flow and ensures REST API tests are always up to date. In addition, REST API tests can be run continuously as part of CI/CD pipeline - an extension of CI/CD pipeline via an additional stage of automatic REST API testing, utilizing REST API test scripts automatically generated based on automatically generated OpenAPI / Swagger spec.


## Technology Stack 

- OpenAPI Generator ([https://openapi-generator.tech/](https://openapi-generator.tech/))
- Swagger / OpenAPI Specification ([https://swagger.io/specification/](https://swagger.io/specification/))
- k6 ([https://k6.io/](https://k6.io/))
- Other

  * OSGi Release 7 Core and Enterprise specifications implementations, including OSGi Configurator, OSGi Declarative Services, OSGi HTTP Whiteboard and OSGi JAX-RS Whiteboard ([https://docs.osgi.org/specification/#release-7](https://docs.osgi.org/specification/#release-7))
  * Gecko.IO ([https://gitlab.com/gecko.io](https://gitlab.com/gecko.io))
  * Pac4J ([https://www.pac4j.org/](https://www.pac4j.org/))
  * EMF ([https://www.eclipse.org/modeling/emf/](https://www.eclipse.org/modeling/emf/))
  * Apache Felix ([https://felix.apache.org/documentation/index.html](https://felix.apache.org/documentation/index.html))
  * MongoDB ([https://www.mongodb.com/](https://www.mongodb.com/))
  * Keycloak ([https://www.keycloak.org/](https://www.keycloak.org/))
  * bnd ([https://bnd.bndtools.org/](https://bnd.bndtools.org/))
  * Gradle ([https://gradle.org/](https://gradle.org/))
  * Docker ([https://www.docker.com/](https://www.docker.com/))
  * Docker Compose ([https://docs.docker.com/compose/](https://docs.docker.com/compose/))
  * Java 11 ([https://docs.oracle.com/en/java/javase/11/](https://docs.oracle.com/en/java/javase/11/))


## Prerequisites

### OpenAPI Generator 

This example project relies on my yet unreleased OpenAPI Generator contributions. In order to make use of these: 

1. Check out `master` branch of OpenAPI Generator repository ([https://github.com/OpenAPITools/openapi-generator](https://github.com/OpenAPITools/openapi-generator))

```
$ git clone https://github.com/OpenAPITools/openapi-generator.git
```


2. Build docker image

```
$ docker build -t openapitools/openapi-generator-cli:latest-unreleased .
```


The `cleaning.dumps.api/generate-tests.sh` script you will find in this example project references thus built Docker image (i.e. `openapitools/openapi-generator-cli:latest-unreleased`).


### Docker and Docker Compose

- [Install Docker](https://docs.docker.com/get-docker/)
- [Install Docker Compose](https://docs.docker.com/compose/install/)
- For Linux, make sure to apply [Post-installation steps for Linux](https://docs.docker.com/engine/install/linux-postinstall/) – at the very least add your user to the `docker` group


### Eclipse IDE

Install Eclipse IDE with Gecko.io add-ons, including Bndtools, Eclipse Modeling Tools and various enhancements, i.e.: 

 - Download Eclipse Installer [https://www.eclipse.org/downloads/packages/installer](https://www.eclipse.org/downloads/packages/installer)
 - Run Eclipse Installer and via "Add User Products" provide the link to the Gecko.io IDE Oomph Installer [https://gitlab.com/gecko.io/geckoBndTemplates/-/raw/develop/cnf/eclipse/GeckoIO-IDE.setup](https://gitlab.com/gecko.io/geckoBndTemplates/-/raw/develop/cnf/eclipse/GeckoIO-IDE.setup), click Next, then specify Installation Folder, Name and Workspace name, as per instructions


Once installed, import this example project into fresh Eclipse workspace ("Import" -> "Existing Projects into Workspace"). Make sure to select the "Search for nested projects" option when importing projects.



## Configuring

The only configuration requirement is a one-time set up for smoke test and load test scripts via environment variables placed in respective `.env` files.

For smoke tests, follow instructions in `cleaning.dumps.api/test/smoke-tests/smoke-tests.env.template`.

For load tests, follow instructions in `cleaning.dumps.api/test/load-tests/load-tests.env.template`


## Generating

### Generate OpenAPI spec

OpenAPI / Swagger spec is generated based on meta-data contained in OpenAPI annotations present in Java code, which is merged with meta-data defined in the `openapi-meta.yaml` file. To generate it, from project root run:

```
$ ./gradlew :cleaning.dumps.api:generateOpenApiSpec --info
```


### Generate REST API tests

#### Generate smoke tests

```
$ cd cleaning.dumps.api

$ ./generate-tests.sh smoke-tests
```

The artifacts you will encounter are: 

1. `test/smoke-tests/smoke-tests.config.yaml` – smoke tests' specific configuration file; no modifications are necessary to this file for scripts to be generated or run; of course you can tweak as necessary if needed, if you know what you are doing;

2. `test/smoke-tests/smoke-tests.env.template` – additional configuration via environment variables; see "Configuring" section above for more information;

3. `test/smoke-tests/templates/script.mustache` – customized script generation template;

4. `test/smoke-tests/templates/README.mustache` – customized documentation generation template;

5. `test/smoke-tests/smoke-tests.script.js` – smoke test script generated automatically, ready to run out-of-the-box;

6. `test/smoke-tests/README.md` – documentation generated automatically, ready to read out-of-the-box ;)


#### Generate load tests

```
$ cd cleaning.dumps.api

$ ./generate-tests.sh load-tests
```

The artifacts you will encounter are:

1. `test/load-tests/load-tests.config.yaml` – load tests' specific configuration file; no modifications are necessary to this file for scripts to be generated or run; of course you can tweak as necessary if needed, if you know what you are doing;

2. `test/load-tests/load-tests.env.template` – additional configuration via environment variables; see "Configuring" section above for more information;

3. `test/load-tests/templates/script.mustache` – customized script generation template;

4. `test/load-tests/templates/README.mustache` – customized documentation generation template;

5. `test/load-tests/load-tests.script.js` – load test script generated automatically, ready to run out-of-the-box;

6. `test/load-tests/README.md` – documentation generated automatically, ready to read out-of-the-box ;)


## Running

### Create and start docker-compose stack

```
$ cd cleaning.dumps.runtime/docker-compose

$ docker-compose --file docker-compose_keycloak.yml up
```

### Resolve and start application 

After loading this example project into Eclipse IDE, in Bndtools view, open the `cleaning.dumps.runtime/cleaning.dumps.runtime.bndrun` run descriptor file.

First, make sure there are no build errors. Then, resolve (i.e. click the `Resolve` button in bottom right corner). If everything is correct with your setup, you should get no resolution errors and a window showing "Resolution Results" should open - accept these via the `Update` button. You are ready to run the application.

To run the application, simply click `Run OSGi` (button in top right corner).


### Run REST API tests

#### Run REST API smoke tests

```
$ cd cleaning.dumps.api/test

$ ./run-smoke-tests.sh
```

#### Run REST API load tests

```
$ cd cleaning.dumps.api/test

$ ./run-load-tests.sh
```


## Project structure

- `cnf` – here you will find bnd workspace configuration

- `cleaning.dumps.api` – in `src` sub-folder you will find REST API endpoint with OpenAPI annotations, including all of the enhancements, demonstrating usage of these new custom OpenAPI extensions; in `openapi` sub-folder you will find automatically generated OpenAPI spec; in `test` sub-folder you will find REST API tests and related artifacts; in root of this bundle you will find the `generate-tests.sh` wrapper script mentioned above (see "Generating") as well as the `openapi-meta.yaml` file containing meta-data merged with meta-data contained in OpenAPI annotations present in Java code

- `cleaning.dumps.config` – in `configs` sub-folder you will find OSGi configuration files

- `cleaning.dumps.model` – in `model` sub-folder you will find EMF model

- `cleaning.dumps.runtime` – in `docker-compose` sub-folder you will find Docker Compose stack definition files; in root of this bundle, you will find the `cleaning.dumps.runtime.bndrun` run descriptor file mentioned above (see "Resolve and start application")

- `cleaning.dumps.security` – in `src` sub-folder you will find security-related code

- `cleaning.dumps.service` – in `src` sub-folder you will find service-layer code

- `cleaning.dumps.service.test` – here you will find service-layer integration test


## Developers

* **Michael H. Siemaszko** (mhs) / [mhs@into.software](mailto:mhs@into.software) @ [Ideas Into Software LLC](https://ideas.into.software) – *software engineer*


## Licenses

**Eclipse Public License 2.0**


## Copyright

[Ideas Into Software LLC](https://ideas.into.software) – All rights reserved
