/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package cleaning.dumps.service.api;

import org.osgi.annotation.versioning.ProviderType;

import cleaning.dumps.model.Dump;
import cleaning.dumps.model.DumpList;

@ProviderType
public interface DumpService {

	Dump saveDump(Dump dump);

	DumpList getDumps();

	DumpList getCleanedDumps();

	Dump getDump(String id);

	Dump markDumpAsCleaned(String id);

	boolean deleteDump(String id);

	boolean dumpExists(String id);
}
