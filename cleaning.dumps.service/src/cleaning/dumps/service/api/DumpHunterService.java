/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package cleaning.dumps.service.api;

import cleaning.dumps.model.DumpHunter;

public interface DumpHunterService {

	DumpHunter createDumpHunterIfNotExists(String dumpHunterId, String dumpHunterName);

	DumpHunter saveDumpHunter(String dumpHunterId, String dumpHunterName);

	DumpHunter saveDumpHunter(DumpHunter dumpHunter);

	DumpHunter getDumpHunter(String dumpHunterId);

	boolean dumpHunterExists(String dumpHunterId);

}
