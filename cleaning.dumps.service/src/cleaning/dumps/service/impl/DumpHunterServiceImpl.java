/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package cleaning.dumps.service.impl;

import java.util.Objects;
import java.util.Optional;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.gecko.emf.repository.EMFRepository;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceScope;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;

import cleaning.dumps.model.CleaningDumpsFactory;
import cleaning.dumps.model.CleaningDumpsPackage;
import cleaning.dumps.model.DumpHunter;
import cleaning.dumps.service.api.DumpHunterService;

/**
 * @see cleaning.dumps.service.api.DumpHunterService
 * @author mhs
 * @since Oct 4, 2021
 */
@Component(name = "DumpHunterService", scope = ServiceScope.PROTOTYPE)
public class DumpHunterServiceImpl implements DumpHunterService {

	@Reference(service = LoggerFactory.class)
	private Logger logger;

	@Reference(target = "(repo_id=cleaningDumps.cleaningDumps)", scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private EMFRepository repository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cleaning.dumps.service.api.DumpHunterService#saveDumpHunter(cleaning.dumps.
	 * model.DumpHunter)
	 */
	@Override
	public DumpHunter saveDumpHunter(DumpHunter dumpHunter) {
		Objects.requireNonNull(dumpHunter, "Dump Hunter is required!");

		repository.save(dumpHunter);

		logger.debug(String.format("Saved dump hunter ID %s", dumpHunter.getId()));

		return EcoreUtil.copy(dumpHunter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cleaning.dumps.service.api.DumpHunterService#saveDumpHunter(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public DumpHunter saveDumpHunter(String dumpHunterId, String dumpHunterName) {
		Objects.requireNonNull(dumpHunterId, "Dump Hunter ID is required!");
		Objects.requireNonNull(dumpHunterName, "Dump Hunter name is required!");

		DumpHunter dumpHunter = CleaningDumpsFactory.eINSTANCE.createDumpHunter();
		dumpHunter.setId(dumpHunterId);
		dumpHunter.setName(dumpHunterName);
		return saveDumpHunter(dumpHunter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cleaning.dumps.service.api.DumpHunterService#getDumpHunter(java.lang.String)
	 */
	@Override
	public DumpHunter getDumpHunter(String dumpHunterId) {
		Objects.requireNonNull(dumpHunterId, "Dump Hunter ID is required!");

		DumpHunter dumpHunter = repository.getEObject(CleaningDumpsPackage.Literals.DUMP_HUNTER, dumpHunterId);
		if (Objects.nonNull(dumpHunter)) {
			logger.debug(String.format("Retrieved dump hunter ID %s", dumpHunter.getId()));
			return EcoreUtil.copy(dumpHunter);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cleaning.dumps.service.api.DumpHunterService#createDumpHunterIfNotExists(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public DumpHunter createDumpHunterIfNotExists(String dumpHunterId, String dumpHunterName) {
		Objects.requireNonNull(dumpHunterId, "Dump Hunter ID is required!");
		Objects.requireNonNull(dumpHunterName, "Dump Hunter name is required!");

		if (!dumpHunterExists(dumpHunterId)) {
			return saveDumpHunter(dumpHunterId, dumpHunterName);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cleaning.dumps.service.api.DumpHunterService#dumpHunterExists(java.lang.
	 * String)
	 */
	@Override
	public boolean dumpHunterExists(String dumpHunterId) {
		Objects.requireNonNull(dumpHunterId, "Dump Hunter ID is required!");

		Optional<DumpHunter> existingDumpHunter = Optional.ofNullable(getDumpHunter(dumpHunterId));
		return existingDumpHunter.isPresent();
	}
}
