/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package cleaning.dumps.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.gecko.emf.repository.EMFRepository;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceScope;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;

import cleaning.dumps.model.CleaningDumpsFactory;
import cleaning.dumps.model.CleaningDumpsPackage;
import cleaning.dumps.model.Dump;
import cleaning.dumps.model.DumpList;
import cleaning.dumps.service.api.DumpService;

/**
 * @see cleaning.dumps.service.api.DumpService
 * @author mhs
 * @since Oct 4, 2021
 */
@Component(name = "DumpService", scope = ServiceScope.PROTOTYPE)
public class DumpServiceImpl implements DumpService {

	@Reference(service = LoggerFactory.class)
	private Logger logger;

	@Reference(target = "(repo_id=cleaningDumps.cleaningDumps)", scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private EMFRepository repository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cleaning.dumps.service.api.DumpService#reportDump(cleaning.dumps.model.Dump)
	 */
	@Override
	public Dump saveDump(Dump dump) {
		Objects.requireNonNull(dump, "Dump is required!");

		repository.save(dump);

		logger.debug(String.format("Saved dump ID %s", dump.getId()));

		return EcoreUtil.copy(dump);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cleaning.dumps.service.api.DumpService#getDumps()
	 */
	@Override
	public DumpList getDumps() {
		DumpList dumpList = CleaningDumpsFactory.eINSTANCE.createDumpList();
		List<Dump> dumps = repository.getAllEObjects(CleaningDumpsPackage.Literals.DUMP);
		if (!dumps.isEmpty()) {
			dumps = dumps.stream().map(d -> EcoreUtil.copy(d)).collect(Collectors.toList());
			dumpList.getDumps().addAll(dumps);

			logger.debug(String.format("Retrieved %d dump(s)", dumps.size()));
		}

		return dumpList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cleaning.dumps.service.api.DumpService#getCleanedDumps()
	 */
	@Override
	public DumpList getCleanedDumps() {
		DumpList dumpList = CleaningDumpsFactory.eINSTANCE.createDumpList();
		List<Dump> dumps = repository.getAllEObjects(CleaningDumpsPackage.Literals.DUMP);
		if (!dumps.isEmpty()) {
			dumps = dumps.stream().filter(d -> d.getCleanedDate() != null).map(d -> EcoreUtil.copy(d))
					.collect(Collectors.toList());
			dumpList.getDumps().addAll(dumps);

			logger.debug(String.format("Retrieved %d cleaned dump(s)", dumps.size()));
		}

		return dumpList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cleaning.dumps.service.api.DumpService#getDump(java.lang.String)
	 */
	@Override
	public Dump getDump(String id) {
		Objects.requireNonNull(id, "Dump ID is required!");

		Dump dump = repository.getEObject(CleaningDumpsPackage.Literals.DUMP, id);
		if (Objects.nonNull(dump)) {
			logger.debug(String.format("Retrieved dump ID %s", dump.getId()));
			return EcoreUtil.copy(dump);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cleaning.dumps.service.api.DumpService#markDumpAsCleaned(java.lang.String)
	 */
	@Override
	public Dump markDumpAsCleaned(String id) {
		Objects.requireNonNull(id, "Dump ID is required!");

		Dump dump = getDump(id);
		if (Objects.nonNull(dump)) {
			dump.setCleanedDate(new Date());
			logger.debug(String.format("Marked dump ID %s as cleaned", dump.getId()));
			return saveDump(dump);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cleaning.dumps.service.api.DumpService#deleteDump(java.lang.String)
	 */
	@Override
	public boolean deleteDump(String id) {
		Objects.requireNonNull(id, "Dump ID is required!");

		Dump dump = getDump(id);
		if (Objects.nonNull(dump)) {
			repository.delete(dump);
			logger.debug(String.format("Deleted dump ID %s", id));
			return true;
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cleaning.dumps.service.api.DumpService#dumpExists(java.lang.String)
	 */
	@Override
	public boolean dumpExists(String id) {
		Objects.requireNonNull(id, "Dump ID is required!");

		Optional<Dump> existingDump = Optional.ofNullable(getDump(id));
		return existingDump.isPresent();
	}

}
