/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package cleaning.dumps.service.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.gecko.mongo.osgi.MongoDatabaseProvider;

import com.mongodb.client.MongoDatabase;

public abstract class AbstractDumpIntegrationTest {
	protected static final String DB_FILTER = "(database=cleaningDumps)";

	protected void dbCleanup(MongoDatabaseProvider dbProvider) {
		assertThat(dbProvider).isNotNull();

		MongoDatabase database = dbProvider.getDatabase();
		database.drop();
	}
}
