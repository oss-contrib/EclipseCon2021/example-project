/**
 * Copyright (c) 2021 Ideas Into Software LLC
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package cleaning.dumps.service.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.gecko.mongo.osgi.MongoDatabaseProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.annotation.Testable;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.test.common.annotation.InjectBundleContext;
import org.osgi.test.common.annotation.InjectService;
import org.osgi.test.common.service.ServiceAware;
import org.osgi.test.junit5.context.BundleContextExtension;
import org.osgi.test.junit5.service.ServiceExtension;

import cleaning.dumps.model.CleaningDumpsFactory;
import cleaning.dumps.model.Dump;
import cleaning.dumps.model.DumpHunter;
import cleaning.dumps.model.DumpList;
import cleaning.dumps.model.DumpLocation;
import cleaning.dumps.service.api.DumpHunterService;
import cleaning.dumps.service.api.DumpService;

@Testable
@ExtendWith(BundleContextExtension.class)
@ExtendWith(ServiceExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DumpServiceIntegrationTest extends AbstractDumpIntegrationTest {

	@InjectBundleContext
	BundleContext bundleContext;

	@BeforeEach
	public void cleanup(
			@InjectService(cardinality = 1, timeout = 2000, filter = DB_FILTER) ServiceAware<MongoDatabaseProvider> dbProviderAware) {

		assertThat(dbProviderAware.getServices()).hasSize(1);
		MongoDatabaseProvider dbProvider = dbProviderAware.getService();
		assertThat(dbProvider).isNotNull();

		super.dbCleanup(dbProvider);
	}

	@Order(value = -1)
	@Test
	public void testServices(
			@InjectService(cardinality = 1, timeout = 2000, filter = DB_FILTER) ServiceAware<MongoDatabaseProvider> dbProviderAware,
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpService> dumpServiceAware,
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpHunterService> dumpHunterServiceAware) {

		assertThat(dbProviderAware.getServices()).hasSize(1);
		ServiceReference<MongoDatabaseProvider> dbProviderReference = dbProviderAware.getServiceReference();
		assertThat(dbProviderReference).isNotNull();

		assertThat(dumpServiceAware.getServices()).hasSize(1);
		ServiceReference<DumpService> dumpServiceReference = dumpServiceAware.getServiceReference();
		assertThat(dumpServiceReference).isNotNull();

		assertThat(dumpHunterServiceAware.getServices()).hasSize(1);
		ServiceReference<DumpHunterService> dumpHunterServiceReference = dumpHunterServiceAware.getServiceReference();
		assertThat(dumpHunterServiceReference).isNotNull();
	}

	@Test
	public void testSaveDump(
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpHunterService> dumpHunterServiceAware,
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpService> dumpServiceAware) {

		assertThat(dumpHunterServiceAware.getServices()).hasSize(1);
		DumpHunterService dumpHunterService = dumpHunterServiceAware.getService();
		assertThat(dumpHunterService).isNotNull();

		assertThat(dumpServiceAware.getServices()).hasSize(1);
		DumpService dumpService = dumpServiceAware.getService();
		assertThat(dumpService).isNotNull();

		DumpHunter dumpHunter = CleaningDumpsFactory.eINSTANCE.createDumpHunter();
		dumpHunter.setName("Dump Hound");
		dumpHunter = dumpHunterService.saveDumpHunter(dumpHunter);

		Dump dump = CleaningDumpsFactory.eINSTANCE.createDump();
		dump.setName("Ugly Dump");

		DumpLocation dumpLocation = CleaningDumpsFactory.eINSTANCE.createDumpLocation();
		dumpLocation.setLatitude(50.064992);
		dumpLocation.setLongitude(19.723404);
		dump.setLocation(dumpLocation);

		dump.setReportedDate(new Date());

		dump.setHunterId(dumpHunter.getId());

		Dump savedDump = dumpService.saveDump(dump);
		assertThat(savedDump).isNotNull();
		assertThat(savedDump.getId()).isNotNull();
	}

	@Test
	public void testGetDumps(
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpHunterService> dumpHunterServiceAware,
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpService> dumpServiceAware) {

		assertThat(dumpServiceAware.getServices()).hasSize(1);
		DumpService dumpService = dumpServiceAware.getService();
		assertThat(dumpService).isNotNull();

		assertThat(dumpHunterServiceAware.getServices()).hasSize(1);
		DumpHunterService dumpHunterService = dumpHunterServiceAware.getService();
		assertThat(dumpHunterService).isNotNull();

		DumpHunter dumpHunter = CleaningDumpsFactory.eINSTANCE.createDumpHunter();
		dumpHunter.setName("Dump Hound");
		dumpHunter = dumpHunterService.saveDumpHunter(dumpHunter);

		// Ugly Dump
		Dump uglyDump = CleaningDumpsFactory.eINSTANCE.createDump();
		uglyDump.setName("Ugly Dump");

		DumpLocation uglyDumpLocation = CleaningDumpsFactory.eINSTANCE.createDumpLocation();
		uglyDumpLocation.setLatitude(50.064992);
		uglyDumpLocation.setLongitude(19.723404);
		uglyDump.setLocation(uglyDumpLocation);

		uglyDump.setReportedDate(new Date());

		uglyDump.setHunterId(dumpHunter.getId());

		uglyDump = dumpService.saveDump(uglyDump);
		assertThat(uglyDump).isNotNull();
		assertThat(uglyDump.getId()).isNotNull();

		// Another Ugly Dump
		Dump anotherUglyDump = CleaningDumpsFactory.eINSTANCE.createDump();
		anotherUglyDump.setName("Another Ugly Dump");

		DumpLocation anotherUglyDumpLocation = CleaningDumpsFactory.eINSTANCE.createDumpLocation();
		anotherUglyDumpLocation.setLatitude(50.076819);
		anotherUglyDumpLocation.setLongitude(19.713337);
		anotherUglyDump.setLocation(anotherUglyDumpLocation);

		anotherUglyDump.setReportedDate(new Date());

		anotherUglyDump.setHunterId(dumpHunter.getId());

		anotherUglyDump = dumpService.saveDump(anotherUglyDump);
		assertThat(anotherUglyDump).isNotNull();
		assertThat(anotherUglyDump.getId()).isNotNull();

		DumpList dumps = dumpService.getDumps();
		assertThat(dumps).isNotNull();
		assertThat(dumps.getDumps().isEmpty()).isFalse();
		assertThat(dumps.getDumps()).hasSize(2);
	}

	@Test
	public void testGetDump(
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpHunterService> dumpHunterServiceAware,
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpService> dumpServiceAware) {

		assertThat(dumpServiceAware.getServices()).hasSize(1);
		DumpService dumpService = dumpServiceAware.getService();
		assertThat(dumpService).isNotNull();

		assertThat(dumpHunterServiceAware.getServices()).hasSize(1);
		DumpHunterService dumpHunterService = dumpHunterServiceAware.getService();
		assertThat(dumpHunterService).isNotNull();

		DumpHunter dumpHunter = CleaningDumpsFactory.eINSTANCE.createDumpHunter();
		dumpHunter.setName("Dump Hound");
		dumpHunter = dumpHunterService.saveDumpHunter(dumpHunter);

		Dump dump = CleaningDumpsFactory.eINSTANCE.createDump();
		dump.setName("Ugly Dump");

		DumpLocation dumpLocation = CleaningDumpsFactory.eINSTANCE.createDumpLocation();
		dumpLocation.setLatitude(50.064992);
		dumpLocation.setLongitude(19.723404);
		dump.setLocation(dumpLocation);

		dump.setReportedDate(new Date());

		dump.setHunterId(dumpHunter.getId());

		dump = dumpService.saveDump(dump);
		assertThat(dump.getId()).isNotNull();

		Dump retrievedDump = dumpService.getDump(dump.getId());
		assertThat(retrievedDump).isNotNull();
		assertThat(retrievedDump.getName()).isEqualTo(dump.getName());
	}

	@Test
	public void testMarkDumpAsCleaned(
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpHunterService> dumpHunterServiceAware,
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpService> dumpServiceAware) {

		assertThat(dumpServiceAware.getServices()).hasSize(1);
		DumpService dumpService = dumpServiceAware.getService();
		assertThat(dumpService).isNotNull();

		assertThat(dumpHunterServiceAware.getServices()).hasSize(1);
		DumpHunterService dumpHunterService = dumpHunterServiceAware.getService();
		assertThat(dumpHunterService).isNotNull();

		DumpHunter dumpHunter = CleaningDumpsFactory.eINSTANCE.createDumpHunter();
		dumpHunter.setName("Dump Hound");
		dumpHunter = dumpHunterService.saveDumpHunter(dumpHunter);

		Dump dump = CleaningDumpsFactory.eINSTANCE.createDump();
		dump.setName("Ugly Dump");

		DumpLocation dumpLocation = CleaningDumpsFactory.eINSTANCE.createDumpLocation();
		dumpLocation.setLatitude(50.064992);
		dumpLocation.setLongitude(19.723404);
		dump.setLocation(dumpLocation);

		dump.setReportedDate(new Date());

		dump.setHunterId(dumpHunter.getId());

		dump = dumpService.saveDump(dump);
		assertThat(dump.getId()).isNotNull();

		Dump cleanedDump = dumpService.markDumpAsCleaned(dump.getId());
		assertThat(cleanedDump).isNotNull();
		assertThat(cleanedDump.getCleanedDate()).isNotNull();
	}

	@Test
	public void testDeleteDump(
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpHunterService> dumpHunterServiceAware,
			@InjectService(cardinality = 1, timeout = 2000) ServiceAware<DumpService> dumpServiceAware) {

		assertThat(dumpServiceAware.getServices()).hasSize(1);
		DumpService dumpService = dumpServiceAware.getService();
		assertThat(dumpService).isNotNull();

		assertThat(dumpHunterServiceAware.getServices()).hasSize(1);
		DumpHunterService dumpHunterService = dumpHunterServiceAware.getService();
		assertThat(dumpHunterService).isNotNull();

		DumpHunter dumpHunter = CleaningDumpsFactory.eINSTANCE.createDumpHunter();
		dumpHunter.setName("Dump Hound");
		dumpHunter = dumpHunterService.saveDumpHunter(dumpHunter);

		Dump dump = CleaningDumpsFactory.eINSTANCE.createDump();
		dump.setName("Ugly Dump");

		DumpLocation dumpLocation = CleaningDumpsFactory.eINSTANCE.createDumpLocation();
		dumpLocation.setLatitude(50.064992);
		dumpLocation.setLongitude(19.723404);
		dump.setLocation(dumpLocation);

		dump.setReportedDate(new Date());

		dump.setHunterId(dumpHunter.getId());

		dump = dumpService.saveDump(dump);
		assertThat(dump.getId()).isNotNull();

		boolean isDeleted = dumpService.deleteDump(dump.getId());
		assertThat(isDeleted).isTrue();
	}
}
