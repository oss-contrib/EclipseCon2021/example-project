/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package cleaning.dumps.api;

import javax.ws.rs.core.Application;

import org.gecko.emf.osgi.json.annotation.RequireEMFJson;
import org.gecko.emf.osgi.rest.jaxrs.annotation.RequireEMFMessageBodyReaderWriter;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsApplicationBase;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsName;

@JaxrsApplicationBase(CleaningDumpsApiConstants.VERSION)
@JaxrsName(CleaningDumpsApiConstants.APP_NAME)
@Component(service = Application.class, property = "emf=true")
@RequireEMFJson
@RequireEMFMessageBodyReaderWriter
public class CleaningDumpsApplication extends Application {
}
