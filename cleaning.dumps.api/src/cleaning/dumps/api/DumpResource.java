/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package cleaning.dumps.api;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gecko.emf.osgi.rest.jaxrs.annotation.ValidateContent;
import org.gecko.emf.osgi.rest.jaxrs.annotation.json.RootElement;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceScope;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.http.whiteboard.annotations.RequireHttpWhiteboard;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsName;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.jax.rs.annotations.Pac4JProfile;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;

import cleaning.dumps.model.Dump;
import cleaning.dumps.model.DumpList;
import cleaning.dumps.security.CleaningDumpsSecurityConstants;
import cleaning.dumps.security.CleaningDumpsSecurityUtil;
import cleaning.dumps.service.api.DumpHunterService;
import cleaning.dumps.service.api.DumpService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.extensions.Extension;
import io.swagger.v3.oas.annotations.extensions.ExtensionProperty;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RequireHttpWhiteboard
@JaxrsResource
@JaxrsName("Dump Resource")
@CleaningDumpsApplicationTarget
@Component(name = "DumpResource", service = DumpResource.class, enabled = true, scope = ServiceScope.PROTOTYPE)
@Path("/dump")
public class DumpResource {

	@Reference(service = LoggerFactory.class)
	private Logger logger;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private DumpHunterService dumpHunterService;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private DumpService dumpService;

	@GET
	@Path("{dumpId}")
	@Pac4JSecurity(clients = CleaningDumpsSecurityConstants.PAC4J_CLIENT_ID)
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(description = "Retrieve existing Dump by its ID",
	// @formatter:off
			responses = {
					@ApiResponse(responseCode = "200", description = "Returns existing Dump by its ID", content = {
							@Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = Dump.class)) }),
					
					@ApiResponse(responseCode = "401", description = "Returned if no token or invalid token is provided or required authentication profile data is missing."),
					
					@ApiResponse(responseCode = "404", description = "Returned if Dump with given ID does not exist.") }, 
			extensions = { 
					@Extension(name = "k6-openapi-operation-grouping", properties = {
							@ExtensionProperty(name = "basic_flow", value = "2")}), 
					// if we use the 'dataextract' option, then example at parameter level (see `dumpId` parameter below) is obviously ignored
					@Extension(name = "k6-openapi-operation-dataextract", properties = {
							@ExtensionProperty(name = "operationId", value = "addDump"), 
							@ExtensionProperty(name = "valuePath", value = "id"), 
							@ExtensionProperty(name = "parameterName", value = "dumpId")})
			},			
			tags = { "dump" })
	// @formatter:on
	public Response getDumpById(@Pac4JProfile @Parameter(hidden = true) CommonProfile authProfile,
			@NotBlank(message = "Please provide dump ID") @PathParam(value = "dumpId") @Parameter(name = "dumpId", example = "616360cbcf683439acd7e273") String dumpId) {

		if (!CleaningDumpsSecurityUtil.INSTANCE.isAuthProfileValid(authProfile)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}

		Optional<Dump> dump = Optional.ofNullable(dumpService.getDump(dumpId));

		if (dump.isEmpty()) {
			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(dump.get()).build();
	}

	@POST
	@Pac4JSecurity(clients = CleaningDumpsSecurityConstants.PAC4J_CLIENT_ID)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(description = "Add new Dump",
	// @formatter:off
			requestBody = @RequestBody(content = {
					@Content(schema = @Schema(implementation = Dump.class), examples = {
					@ExampleObject(name = "addDumpExample", description = "Add new Dump example",
					value = "{\n"
							+ "  \"name\": \"Another Ugly Dump ...\",\n"
							+ "  \"location\": {\n"
							+ "    \"latitude\": 50.076819,\n"
							+ "    \"longitude\": 19.713337\n"
							+ "  }\n"
							+ "}"
					
					) }) 
			}),	
			// alternatively, we can provide an example with ID and use that ID as an example at parameter level (see `dumpId` for `getDumpById` and `deleteDump` methods, however we will not be able to run this as part of a load test (where simultaneously multiple virtual users are hitting the endpoint)
			/*
			requestBody = @RequestBody(content = {
					@Content(schema = @Schema(implementation = Dump.class), examples = {
					@ExampleObject(name = "addDumpExample", description = "Add new Dump example",
					value = "{\n"
							+ "  \"id\": \"616360cbcf683439acd7e273\",\n"
							+ "  \"name\": \"Another Ugly Dump\",\n"
							+ "  \"location\": {\n"
							+ "    \"latitude\": 50.076819,\n"
							+ "    \"longitude\": 19.713337\n"
							+ "  }\n"
							+ "}"
					) }) 
			}),
			*/ 
			responses = {
					@ApiResponse(responseCode = "200", description = "Returns the saved Dump", content = {
							@Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = Dump.class)) }),
					
					@ApiResponse(responseCode = "401", description = "Returned if no token or invalid token is provided or required authentication profile data is missing.") 
			}, 
			extensions = {
					@Extension(name = "k6-openapi-operation-grouping", properties = {
							@ExtensionProperty(name = "basic_flow", value = "1")
					})
			},
			tags = {"dump" })
	// @formatter:on
	public Response addDump(@Context HttpServletRequest request, @Context HttpServletResponse response,
			@ValidateContent @RootElement(rootClassUri = CleaningDumpsApiConstants.DUMP_URI) Dump dump) {

		if (!CleaningDumpsSecurityUtil.INSTANCE.isAuthProfileValid(request, response)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}

		String dumpHunterId = CleaningDumpsSecurityUtil.INSTANCE.getAuthProfileUserId(request, response);
		String dumpHunterName = CleaningDumpsSecurityUtil.INSTANCE.getAuthProfileUserName(request, response);
		dumpHunterService.createDumpHunterIfNotExists(dumpHunterId, dumpHunterName);

		dump.setHunterId(dumpHunterId);
		dump.setReportedDate(new Date());

		Dump savedDump = dumpService.saveDump(dump);
		if (Objects.isNull(savedDump.getId())) {
			return Response.serverError().build();
		}

		return Response.ok(savedDump).build();
	}

	@GET
	@Pac4JSecurity(clients = CleaningDumpsSecurityConstants.PAC4J_CLIENT_ID)
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(description = "Retrieve list of Dumps",
	// @formatter:off
			responses = {
					@ApiResponse(responseCode = "200", description = "Retrieves list of Dumps", content = {
							@Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = DumpList.class)) }),
					
					@ApiResponse(responseCode = "204", description = "Returned if list of Dumps is empty", 
						extensions = {
								@Extension(name = "k6-openapi-operation-response", properties = {
										@ExtensionProperty(name = "hide", value = "true")
								})
						}),
					
					@ApiResponse(responseCode = "401", description = "Returned if no token or invalid token is provided or required authentication profile data is missing.") 
			}, 
			extensions = {
					@Extension(name = "k6-openapi-operation-grouping", properties = {
							@ExtensionProperty(name = "basic_flow", value = "3")})
			},
			tags = { "dump" })
	// @formatter:on
	public Response listDumps(@Pac4JProfile @Parameter(hidden = true) CommonProfile authProfile,
			@QueryParam("status") @Parameter(name = "status", example = "all") String status) {

		if (!CleaningDumpsSecurityUtil.INSTANCE.isAuthProfileValid(authProfile)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}

		Optional<DumpList> dumps = Optional.empty();

		if (status == null || (status != null && "all".equalsIgnoreCase(status))) {
			dumps = Optional.of(dumpService.getDumps());

		} else if (status != null && "cleaned".equalsIgnoreCase(status)) {
			dumps = Optional.of(dumpService.getCleanedDumps());
		}

		if (dumps.isEmpty() || (dumps.isPresent() && dumps.get().getDumps().isEmpty())) {
			return Response.noContent().build();
		}

		return Response.ok(dumps.get()).build();
	}

	@DELETE
	@Path("{dumpId}")
	@Pac4JSecurity(clients = CleaningDumpsSecurityConstants.PAC4J_CLIENT_ID)
	@Operation(description = "Delete existing Dump",
	// @formatter:off
			responses = {
					@ApiResponse(responseCode = "204", description = "Returned if dump was successfully deleted."),
					
					@ApiResponse(responseCode = "401", description = "Returned if no token or invalid token is provided or required authentication profile data is missing."),
					
					@ApiResponse(responseCode = "404", description = "Returned if Dump with given ID does not exist.") }, 
			extensions = {
					@Extension(name = "k6-openapi-operation-grouping", properties = {
							@ExtensionProperty(name = "basic_flow", value = "4")}),
					// if we use the 'dataextract' option, then example at parameter level (see `dumpId` parameter below) is obviously ignored 
					@Extension(name = "k6-openapi-operation-dataextract", properties = {
							@ExtensionProperty(name = "operationId", value = "addDump"), 
							@ExtensionProperty(name = "valuePath", value = "id"), 
							@ExtensionProperty(name = "parameterName", value = "dumpId")})
			}, 
			tags = { "dump" })
	// @formatter:on
	public Response deleteDump(@Pac4JProfile @Parameter(hidden = true) CommonProfile authProfile,
			@NotBlank(message = "Please provide dump ID") @PathParam(value = "dumpId") @Parameter(name = "dumpId", example = "616360cbcf683439acd7e273") String dumpId) {

		if (!CleaningDumpsSecurityUtil.INSTANCE.isAuthProfileValid(authProfile)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}

		if (dumpService.dumpExists(dumpId)) {
			boolean isDeleted = dumpService.deleteDump(dumpId);
			if (isDeleted) {
				return Response.noContent().build();
			} else {
				return Response.serverError().build();
			}

		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}
}
