/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package cleaning.dumps.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.osgi.service.component.annotations.ComponentPropertyType;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

@ComponentPropertyType
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface CleaningDumpsApplicationTarget {

	/**
	 * Prefix for the property name. This value is prepended to each property name.
	 */
	String PREFIX_ = "osgi.";

	/**
	 * Service property providing an OSGi filter identifying the application(s) to
	 * which this service should be bound.
	 * 
	 * @return The filter for selecting the applications to bind to.
	 * @see org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants#JAX_RS_APPLICATION_SELECT
	 */
	String jaxrs_application_select() default "(" + JaxrsWhiteboardConstants.JAX_RS_NAME + "="
			+ CleaningDumpsApiConstants.APP_NAME + ")";
}
