/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package cleaning.dumps.api;

public interface CleaningDumpsApiConstants {

	static final String VERSION = "1.0";
	static final String APP_NAME = "CleaningDumpsApplication";
	static final String DUMP_URI = "http://cleaning.dumps/model/1.0#//Dump";
}
