#!/bin/bash -e

docker run --rm \
 --network host \
 --volume ${PWD}:/test \
 --interactive \
 --env-file ./load-tests/load-tests.env \
 loadimpact/k6 \
 run /test/load-tests/load-tests.script.js

