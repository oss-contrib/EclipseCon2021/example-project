
import http from 'k6/http';

/**
 * Authenticate using OAuth2.0 against Keycloak (https://www.keycloak.org/)
 * @function
 * @param  {string} address - Keycloak service address, i.e. hostname, optionally including port number if not using default port (e.g. 'localhost:9080')
 * @param  {string} useSSL - Whether SSL should be used
 * @param  {string} realm - Keycloak realm name
 * @param  {string} clientId - Keycloak client id
 * @param  {string} clientSecret - eycloak client secret
 * @param  {string} scope - Space-separated list of scopes
 * @param  {object} userCredentials - Object containing username and password 
 */
export function authenticateUsingKeycloak(
 address, 
 useSSL,
 realm, 
 clientId,
 clientSecret,
 scope,
 userCredentials) {
 
  if (! (typeof userCredentials == 'object' &&
         userCredentials.hasOwnProperty('username') &&
         userCredentials.hasOwnProperty('password')) ) {
    
    throw 'userCredentials must be an object containing username and password!';
  }
  
  let url = `${useSSL == "true" ? 'https' : 'http'}://${address}/auth/realms/${realm}/protocol/openid-connect/token`;
   
  const requestBody = {
    grant_type: 'password',
    client_id: clientId,
    client_secret: clientSecret,
    scope: scope,
    username: userCredentials.username,
    password: userCredentials.password
  };
  
  let response = http.post(url, requestBody);

  return response.json();
}


