/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package cleaning.dumps.security;

import java.util.Objects;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.pac4j.core.context.J2EContext;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;

public enum CleaningDumpsSecurityUtil {
	INSTANCE;

	public boolean isAuthProfileValid(HttpServletRequest request, HttpServletResponse response) {
		return (Objects.nonNull(request) && Objects.nonNull(response)
				&& isAuthProfileValid(new ProfileManager<CommonProfile>(new J2EContext(request, response))));
	}

	public boolean isAuthProfileValid(ProfileManager<CommonProfile> profileManager) {
		return isAuthProfileValid(profileManager.get(true).get());
	}

	public boolean isAuthProfileValid(CommonProfile authProfile) {
		return (Objects.nonNull(authProfile) && hasAuthProfileUserId(authProfile))
				&& hasAuthProfileUserName(authProfile);
	}

	private boolean hasAuthProfileUserId(CommonProfile authProfile) {
		return (Optional.ofNullable(authProfile.getId()).isPresent());
	}

	private boolean hasAuthProfileUserName(CommonProfile authProfile) {
		return (authProfile.containsAttribute(CleaningDumpsSecurityConstants.PAC4J_PROFILE_USER_NAME) && !authProfile
				.getAttribute(CleaningDumpsSecurityConstants.PAC4J_PROFILE_USER_NAME, String.class).isEmpty());
	}

	public String getAuthProfileUserId(HttpServletRequest request, HttpServletResponse response) {
		return getAuthProfileUserId(new ProfileManager<CommonProfile>(new J2EContext(request, response)));
	}

	public String getAuthProfileUserId(ProfileManager<CommonProfile> profileManager) {
		return getAuthProfileUserId(profileManager.get(true).get());
	}

	public String getAuthProfileUserId(CommonProfile authProfile) {
		return authProfile.getId();
	}

	public String getAuthProfileUserName(HttpServletRequest request, HttpServletResponse response) {
		return getAuthProfileUserName(new ProfileManager<CommonProfile>(new J2EContext(request, response)));
	}

	public String getAuthProfileUserName(ProfileManager<CommonProfile> profileManager) {
		return getAuthProfileUserName(profileManager.get(true).get());
	}

	public String getAuthProfileUserName(CommonProfile authProfile) {
		return authProfile.getAttribute(CleaningDumpsSecurityConstants.PAC4J_PROFILE_USER_NAME, String.class);
	}
}
