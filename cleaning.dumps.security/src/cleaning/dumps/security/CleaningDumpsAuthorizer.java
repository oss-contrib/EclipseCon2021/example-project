/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package cleaning.dumps.security;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.pac4j.core.authorization.authorizer.Authorizer;
import org.pac4j.core.authorization.authorizer.ProfileAuthorizer;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.profile.AnonymousProfile;
import org.pac4j.core.profile.CommonProfile;

/**
 * An Authorizer to check that our user is authenticated. Later, this can be
 * replaced with role-based authorizer.
 * 
 * Based on
 * {@link org.pac4j.core.authorization.authorizer.IsAuthenticatedAuthorizer<U>}
 * 
 * @author mhs
 * @since Oct 5, 2021
 */
@Component(name = "CleaningDumpsAuthorizer", immediate = true)
public class CleaningDumpsAuthorizer extends ProfileAuthorizer<CommonProfile> implements Authorizer<CommonProfile> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.pac4j.core.authorization.authorizer.Authorizer#isAuthorized(org.pac4j.
	 * core.context.WebContext, java.util.List)
	 */
	@Override
	public boolean isAuthorized(WebContext context, List<CommonProfile> profiles) {
		return isAnyAuthorized(context, profiles);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.pac4j.core.authorization.authorizer.ProfileAuthorizer#isProfileAuthorized
	 * (org.pac4j.core.context.WebContext, org.pac4j.core.profile.CommonProfile)
	 */
	@Override
	protected boolean isProfileAuthorized(WebContext context, CommonProfile profile) {
		return profile != null && !(profile instanceof AnonymousProfile);
	}
}
