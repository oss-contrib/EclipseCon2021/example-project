/**
 * Copyright (c) 2021 Ideas Into Software LLC.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 */
package cleaning.dumps.security;

public interface CleaningDumpsSecurityConstants {

	static final String PAC4J_CLIENT_ID = "rest-client";

	static final String PAC4J_PROFILE_USER_NAME = "name";

	static final String KEYCLOAK_CONFIGURATION_PID = "BearerTokenClient~admin";

}
